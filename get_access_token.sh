#!/bin/bash
export $(grep -v '^#' .env | xargs)
curl --request POST \
  --url https://gateway.stackpath.com/identity/v1/oauth2/token \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
    "client_id": "'"$SP_CLIENT_ID"'",
    "client_secret": "'"$SP_CLIENT_SECRET"'",
    "grant_type": "client_credentials"
  }' | jq '.access_token'
