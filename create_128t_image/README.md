# create 128T router image

To speed up deployment of new router VMs, an image can be created and saved, which is
configured as a router (combo), using either a conductor IP provided via env variable
conductor_ip (in file ../.env) or 10.10.10.10 will be used. That conductor IP can be
modified via cloud-init by modifing the file /etc/salt/minion. 

## Steps to produce and save the VM image

- Set your SP// credentials in ../.env
- Set your t128 password in .././env, will be used to log into the VM as admin and root
- Set conductor_ip in ../.env to overwrite the default of 10.10.10.10. It can be overwritten at launch time, so the default is used to find and replace the IP later
- Launch the builder VM
- Check progress via serial console (using SP// Web UI)
- Once the VM completed, it will shut itself down (without power down, to avoid getting relaunched by SP// automatically)
- Save the VM image using the SP// Web UI
- Destroy the builder workload form the SP// Web UI


