#!/bin/bash
export $(grep -v '^#' ../.env | xargs)    # grab SP// secrets

ssh_public_key="${ssh_public_key:-ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbppm0i41M7zMFba3EWdtuxCaomRQzEdonIzXSiETsZ mwiget@may2020}"

if [ ! -e ../my_128t_certificate.pem ]; then
  echo "please create ../my_128t_certificate.pem file first"
  exit 1
fi

conductor_ip="${conductor_ip:-10.10.10.10}"
echo ""
echo "Setting conductor_ip to $conductor_ip"
router_admin_password=$(openssl passwd -6 $t128_password)
router_root_password=$router_admin_password
echo "Setting admin and root password to $t128_password"
echo ""

pem_128t_certificate=$(base64 -w0 < ../my_128t_certificate.pem)

CLOUD_INIT=$(base64 -w0 <<EOF
#cloud-config
user: t128
password: ${t128_password}
chpasswd: {expire: False}

ssh_pwauth: True
ssh_authorized_keys:
  - ${ssh_public_key}

package_update: true
package_upgrade: false
package_reboot_if_required: false

write_files:
  - path: /etc/pki/128technology/release.pem
    append: false
    content: !!binary |
      ${pem_128t_certificate}
  - path: /tmp/preferences.json
    append: false
    content: |
      {
        "install": {
          "initialize": {
            "node-role": "combo",                                                                                                                                      
            "node-name": "MyRouter",                                                                                                                                   
            "router-name": "MyRouter",                                                                                                                                 
            "admin-password": "${router_admin_password}",                                                                                                          
            "root-password": "${router_root_password}",                                                                                                            
            "conductor": {                                                                                                                                             
              "primary": {                                                                                                                                             
                "ip": "${conductor_ip}"                                                                                                                            
              }                                                                                                                                                        
            }
          }
        }
      }

runcmd:
  - yum -y install http://yum.128technology.com/installer/repo.rpm
  - yum -y install 128T-installer
  - install128t --preferences /tmp/preferences.json
  - halt

final_message: "128T installation complete!"
EOF
)

ACCESS_TOKEN=$(curl --silent --request POST \
  --url https://gateway.stackpath.com/identity/v1/oauth2/token \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
    "client_id": "'"$SP_CLIENT_ID"'",
    "client_secret": "'"$SP_CLIENT_SECRET"'",
    "grant_type": "client_credentials"
  }' | jq -r '.access_token') 

STACK_ID=$(curl --silent --request GET \
  --url https://gateway.stackpath.com/stack/v1/stacks \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer $ACCESS_TOKEN" | jq -r '.results[0].id')

echo "creating workload ..."

# hint: to find json parsing errors, echo the command, capture stdout, remove
# everything before data ' and pipe it into `cat x | python3 -m json.tool > /dev/null`

curl --silent --request POST \
  --url https://gateway.stackpath.com/workload/v1/stacks/$STACK_ID/workloads \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer $ACCESS_TOKEN" \
  --data '{
   "workload": {
      "name": "mwiget-128t-image-builder",
      "metadata": {
        "annotations": {
          "workload.platform.stackpath.net/remote-management": "true",
          "workload.platform.stackpath.net/ssh-public-keys": "'"${ssh_public_key}"'"
        }
      },
      "spec": {
        "networkInterfaces": [
          {
            "network": "default"
          }
        ],
        "virtualMachines": {
          "sp-128t": {
            "image": "ext-juniper-integration-31a682/centos75:1804",
            "ports": {},
            "userData": "'"$CLOUD_INIT"'",
            "resources": {
              "requests": {
                "cpu": "4",
                "memory": "16Gi"
              },
              "limits": {}
            }
          }
        }
      },
      "targets": {
        "global": {
          "spec": {
            "deploymentScope": "cityCode",
            "deployments": {
              "minReplicas": 1,
              "selectors": [
                {
                  "key": "cityCode",
                  "operator": "in",
                  "values": [
                    "FRA"
                  ]
                }
              ]
            }
          }
        }
      },
      "slug": "mwiget-128t-image-builder"
    }
  }' | jq
