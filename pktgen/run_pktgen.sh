#!/bin/bash
PCI=0000:05:00.0	# net1

sudo mkdir /mnt/huge
sudo mount -t hugetlbfs hugetlb /mnt/huge

sudo modprobe uio
sudo insmod ./dpdk-20.02/x86_64-native-linuxapp-gcc/kmod/igb_uio.ko

sudo python3 dpdk-20.02/usertools/dpdk-devbind.py --force -u $PCI
sudo python3 dpdk-20.02/usertools/dpdk-devbind.py -b igb_uio $PCI

#sudo python3 dpdk-20.02/usertools/dpdk-devbind.py --status

sudo ./pktgen-20.02.0/app/x86_64-native-linuxapp-gcc/pktgen  -- -T -P -m "[1:2].0" -f test0.pkt --
sudo python3 dpdk-20.02/usertools/dpdk-devbind.py --force -u $PCI
sudo python3 dpdk-20.02/usertools/dpdk-devbind.py -b igb_uio $PCI
sudo python3 dpdk-20.02/usertools/dpdk-devbind.py --status
