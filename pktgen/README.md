# pktgen on sp//

Copy terraform.tfvars.sample terraform.tfvars and fill in the blanks:

```
$ cp terraform.tfvars.sample terraform.tfvars
$ vi terraform.tfvars
```

Deploy 2 VMs using TF:

```
$ terraform init
$ terraform apply
```

run download and installation sript

```
$ bash ./install_pktgen.sh
. . .
 CC pktgen-rate.o
  LD pktgen
  INSTALL-APP pktgen
  INSTALL-MAP pktgen.map
-rwxr-xr-x 1 root root 16714168 Jan 29 10:32 ./pktgen-20.02.0/app/x86_64-native-linuxapp-gcc/pktgen
```

If successful, the path to pktgen is shown in the output. It doesn't get installed elsewhere.

Load the dpdk module on net1 (0000:05:00) via script. If it works, you'll see that PCI address
listed under '...using DPDK-compatible driver':

```
$ ./load_modules.sh 

Network devices using DPDK-compatible driver
============================================
0000:05:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' drv=igb_uio unused=liquidio_vf,vfio-pci

Network devices using kernel driver
===================================
0000:04:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' if=eth0 drv=LiquidIO_VF unused=liquidio_vf,igb_uio,vfio-pci *Active*

No 'Baseband' devices detected
==============================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================

No 'Misc (rawdev)' devices detected
===================================
```

Adjust the mac addresses in test0.pkt and test1.pkt according to the interfaces net1 on either machine.

```
mwiget-pktgen-eu-fra-0:~$ ifconfig net1
net1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1450
        inet 192.168.80.4  netmask 255.255.0.0  broadcast 192.168.255.255
        inet6 fe80::858:c0ff:fea8:5004  prefixlen 64  scopeid 0x20<link>
        ether 0a:58:c0:a8:50:04  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 14  bytes 1132 (1.1 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

mwiget-pktgen-eu-fra-1:~$  ifconfig net1
net1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1450
        inet 192.168.80.5  netmask 255.255.0.0  broadcast 192.168.255.255
        inet6 fe80::858:c0ff:fea8:5005  prefixlen 64  scopeid 0x20<link>
        ether 0a:58:c0:a8:50:05  txqueuelen 1000  (Ethernet)
        RX packets 751505  bytes 1039238285 (1.0 GB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 74024  bytes 6474855 (6.4 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

admin@mwiget-pktgen-eu-fra-0:~$ arp -na
? (192.168.80.5) at 0a:58:c0:a8:50:01 [ether] on net1
? (10.128.160.1) at 0a:58:0a:80:a0:01 [ether] on eth0

admin@mwiget-pktgen-eu-fra-1:~$ arp -na
? (192.168.80.4) at 0a:58:c0:a8:50:01 [ether] on net1
? (10.128.160.1) at 0a:58:0a:80:a0:01 [ether] on eth0
```

Then launch pktgen via script:

```
$ ./run_pktgen.sh
. . .
 Ports 0-0 of 1   <Main Page>  Copyright (c) <2010-2019>, Intel Corporation
  Flags:Port        : P------Single  VLAN:0
Link State          :         <UP-25000-FD>      ---Total Rate---
Pkts/s Max/Rx       :                   0/0                   0/0
       Max/Tx       :                   0/0                   0/0
MBits/s Rx/Tx       :                   0/0                   0/0
Broadcast           :                     0
Multicast           :                     0
Sizes 64            :                     0
      65-127        :                     0
      128-255       :                     0
      256-511       :                     0
      512-1023      :                     0
      1024-1518     :                     0
Runts/Jumbos        :                   0/0
ARP/ICMP Pkts       :                   0/0
Errors Rx/Tx        :                   0/0
Total Rx Pkts       :                     0
      Tx Pkts       :                     0
      Rx MBs        :                     0
      Tx MBs        :                     0
                    :
Pattern Type        :               abcd...
Tx Count/% Rate     :          Forever /20%
Pkt Size/Tx Burst   :             64 /   64
TTL/Port Src/Dest   :         4/ 1234/ 1234
Pkt Type:VLAN ID    :       IPv4 / UDP:0000
802.1p CoS/DSCP/IPP :             0/  0/  0
VxLAN Flg/Grp/vid   :      0000/    0/    0
IP  Destination     :          192.168.80.5
    Source          :          192.168.80.4
MAC Destination     :     0a:58:c0:a8:50:01
    Source          :     0a:58:c0:a8:50:04
PCI Vendor/Addr     :     177d:9712/05:00.0

-- Pktgen 20.02.0 (DPDK 20.02.0)  Powered by DPDK  (pid:1757) -----------------
Pktgen:/> quit

```

Example run between 2 VMs in the same DC, low speed:

![](pktgen_vlan0.jpg)

