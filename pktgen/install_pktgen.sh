#!/usr/bin/bash
dpdk_version=20.02
pktgen_version=20.02.0
DPDK_VER=$dpdk_version
PKTGEN_VER=$pktgen_version
export RTE_TARGET=x86_64-native-linuxapp-gcc
export RTE_SDK=$PWD/dpdk-$DPDK_VER

echo DPDK_VER=${DPDK_VER}
echo PKTGEN_VER=${PKTGEN_VER}
echo RTE_SDK=${RTE_SDK}

# downloading and unpacking DPDK
if [ ! -d $RTE_SDK ]; then
  if [ ! -e dpdk-$DPDK_VER.tar.xz ]; then
    wget -q https://fast.dpdk.org/rel/dpdk-$DPDK_VER.tar.xz 
  fi
  tar xf dpdk-$DPDK_VER.tar.xz
  # the following patch is not required on SP//, but I need it when 
  # using pktgen on an VF SR-IOV interface
  cd $RTE_SDK
  patch -p1 < ../vf_bus_error_fix.diff
  cd ..
fi

echo =========================================================================
echo build and install DPDK ...
echo =========================================================================
cd $RTE_SDK
#make clean
make config T=$RTE_TARGET CONFIG_RTE_EAL_IGB_UIO=y 
make install T=$RTE_TARGET DESTDIR=install CONFIG_RTE_EAL_IGB_UIO=y -j4
cd ..

if [ ! -d pktgen-$PKTGEN_VER ]; then
  echo =========================================================================
  echo download and unpack pktgen
  echo =========================================================================
  if [ ! -e pktgen-$PKTGEN_VER.tar.gz ]; then
    wget -q https://dpdk.org/browse/apps/pktgen-dpdk/snapshot/pktgen-$PKTGEN_VER.tar.gz
  fi
  tar xf pktgen-$PKTGEN_VER.tar.gz
  cd pktgen-$PKTGEN_VER/app
  patch -p0 <../../pktgen-main.diff
  cd ../..
fi

echo =========================================================================
echo building pktgen ...
echo =========================================================================

cd pktgen-$PKTGEN_VER 
#make clean
make -j4

cd ..
ls -l ./pktgen-20.02.0/app/x86_64-native-linuxapp-gcc/pktgen
