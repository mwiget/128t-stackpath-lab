resource "stackpath_compute_workload" "pktgen" {
  name = "mwiget-pktgen"
  slug = "mwiget-pktgen"

  annotations = {
   # don't request an anycast IP by commenting out the following line
   #   "anycast.platform.stackpath.net" = "true"
  }

  network_interface {
   network = "default"
  }

  network_interface {
   network = "vnf-net-1"
  }

  virtual_machine {

    name = "c"
    image = "stackpath-edge/ubuntu-2004-focal:v202007291705"

    resources {
      requests = {
        "cpu" = "4"
        "memory" = "16Gi"
      }
    }

    # The ports that should be publicly exposed on the VM.
    port {
      name = "ssh"
      port = 22
      protocol = "TCP"
      enable_implicit_network_policy = true
    }

    user_data = <<EOT
#cloud-config
user: admin
password: ${var.admin_password}
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ${var.ssh_public_key}

package_reboot_if_required: false

runcmd:
  - apt-get update
  - DEBIAN_FRONTEND=noninteractive apt-get -y install build-essential python3-pip liblua5.3-dev wget libnuma-dev pciutils libpcap-dev libelf-dev linux-headers-`uname -r` git net-tools
  - ln -s /usr/bin/python3 /usr/bin/python
  - sed -i 's/tty0"/tty0 hugepages=2048 iommu=pt intel_iommu=on"/' /etc/default/grub
  - /usr/sbin/update-grub2
  - cd /home/admin && git clone https://gitlab.com/mwiget/128t-stackpath-lab.git
  - cd /home/admin/128t-stackpath-lab/pktgen && /usr/bin/bash install_pktgen.sh
  - reboot

final_message: "The system is ready"
EOT

  }

  target {
    name = "eu"
    min_replicas = 2
    max_replicas = 2
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "FRA"
      ]
    }
  }
}

output "pktgen_instances" {
  value = {
    for instance in stackpath_compute_workload.pktgen.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "pktgen_instance_ips" {
  value = [stackpath_compute_workload.pktgen.instances.*.external_ip_address]
}

