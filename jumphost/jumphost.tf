resource "stackpath_compute_workload" "jumphost" {
  name = "mwiget-jumphost"
  slug = "mwiget-jumphost"

  annotations = {
   # don't request an anycast IP by commenting out the following line
  #   "anycast.platform.stackpath.net" = "true"
  }

  network_interface {
   network = "default"
  }

  network_interface {
   network = "vnf-net-1"
  }

  virtual_machine {

    name = "c"
    image = "stackpath-edge/ubuntu-2004-focal:v202102241556"

    resources {
      requests = {
        "cpu" = "1"
        "memory" = "2Gi"
      }
    }

    # The ports that should be publicly exposed on the VM.
    port {
      name = "ssh"
      port = 22
      protocol = "TCP"
      enable_implicit_network_policy = true
    }

    user_data = <<EOT
#cloud-config
user: admin
password: ${var.router_admin_password}
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ${var.ssh_public_key}

package_update: true
package_upgrade: false
package_reboot_if_required: true

users:
  - name: admin
    passwd: ${var.router_admin_password}
    ssh_authorized_keys:
      - ${var.ssh_public_key}

EOT

  }

  target {
    name = "us"
    min_replicas = 1
    max_replicas = 1
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "DFW"
      ]
    }
  }
}

output "jumphost_instances" {
  value = {
    for instance in stackpath_compute_workload.jumphost.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "jumphost_instance_ips" {
  value = [stackpath_compute_workload.jumphost.instances.*.external_ip_address]
}

output "conductor_ip" {
  value = var.conductor_ip
}
