#!/bin/bash
export $(grep -v '^#' ../.env | xargs)

sed 's/ = /=/' terraform.tfvars  > /tmp/vars.$$
sed -i 's/pem_128t_certificate=<<-EOF/pem_128t_certificate=$(cat <<EOF/' /tmp/vars.$$
sed -i 's/^EOF/EOF\n)/' /tmp/vars.$$
sed -i "s/\"/\'/g" /tmp/vars.$$
source /tmp/vars.$$
rm /tmp/vars.$$

CLOUD_INIT=$(base64 -w0 <<EOF
#cloud-config
user: admin
password: ${router_admin_password}
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ${ssh_public_key}

package_update: true
package_upgrade: true
package_reboot_if_required: true

write_files:
  - path: /tmp/preferences.json
    append: false
    content: |
      {
        "install": {
          "initialize": {
            "node-role": "combo",
            "node-name": "MyRouter",
            "router-name": "MyRouter",
            "admin-password": "${router_admin_password}",
            "root-password": "${router_root_password}",
            "conductor": {
              "primary": {
                "ip": "${conductor_ip}"
              }
            }
          }
        }
      }
  - path: /etc/pki/128technology/release.pem
    append: false
    content: !!binary |
      ${pem_128t_certificate}

package_upgrade: false

runcmd:
  - install128t --preferences /tmp/preferences.json
  - systemctl enable 128T
  - reboot

final_message: "The system is finally up, after $UPTIME seconds"
EOF
)

ACCESS_TOKEN=$(curl --silent --request POST \
  --url https://gateway.stackpath.com/identity/v1/oauth2/token \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
    "client_id": "'"$SP_CLIENT_ID"'",
    "client_secret": "'"$SP_CLIENT_SECRET"'",
    "grant_type": "client_credentials"
  }' | jq -r '.access_token') 

STACK_ID=$(curl --silent --request GET \
  --url https://gateway.stackpath.com/stack/v1/stacks \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer $ACCESS_TOKEN" | jq -r '.results[0].id')

echo "creating workloads ..."

curl --silent --request POST \
  --url https://gateway.stackpath.com/workload/v1/stacks/$STACK_ID/workloads \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer $ACCESS_TOKEN" \
  --data '{
   "workload": {
      "name": "mwiget-router",
      "metadata": {
        "annotations": {
          "anycast.platform.stackpath.net": "true"
        }
      },
      "spec": {
        "networkInterfaces": [
          {
            "network": "default"
          },
          {
            "network": "vnf-net-1",
            "enableOneToOneNat": true
          },
          {
            "network": "vnf-net-2",
            "enableOneToOneNat": true
          }
        ],
        "virtualMachines": {
          "sp-128t": {
            "image": "ext-juniper-integration-31a682/centos75-128t:v20210123",
            "ports": {},
            "userData": "'"$CLOUD_INIT"'",
            "resources": {
              "requests": {
                "cpu": "4",
                "memory": "16Gi"
              },
              "limits": {}
            }
          }
        }
      },
      "targets": {
        "global": {
          "spec": {
            "deploymentScope": "cityCode",
            "deployments": {
              "minReplicas": 1,
              "selectors": [
                {
                  "key": "cityCode",
                  "operator": "in",
                  "values": [
                    "DFW"
                  ]
                }
              ]
            }
          }
        }
      },
      "slug": "mwiget-router"
    }
  }'

echo "creating network policy via terraform ..."
terraform apply --auto-approve
