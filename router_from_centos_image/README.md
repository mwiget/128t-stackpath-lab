

Plan to use the following network interfaces:

- eth0: WAN connected to default network, used as WAN interface, as it has a public IP
- net1: mgmt interface, connecting to conductor
- net2: future connectivity to cloud based private lab


```
$ terraform apply
```

After several minutes, the routers reboot and I can log back in:

```
[root@mwiget-router-eu-dfw-0 admin]# 128tok.sh

128T System Information:
-----------------------------
Kernel version:               3.10.0-1160.el7.x86_64
Total Cores:                  4
Assigned Cores:               3
Unassigned(DPDK):             1
Context:                      run
Derived Role:                 combo
Specified Role:               
Source:                       all

128T System Checks:
-----------------------------
check_cpu_vendor:             PASS CPU VendorID: GenuineIntel (must be Intel)
check_cpu_flags:              PASS CPU 0 core 0 has flags ssse3,sse4_1,sse4_2
check_cpu_flags:              PASS CPU 1 core 1 has flags ssse3,sse4_1,sse4_2
check_cpu_flags:              PASS CPU 2 core 2 has flags ssse3,sse4_1,sse4_2
check_cpu_flags:              PASS CPU 3 core 3 has flags ssse3,sse4_1,sse4_2
check_selinux_status:         PASS SELinux status is cfg:disabled,run:disabled
check_system_memory:          PASS 16 GB of Required Minimum of 3 GB
check_swap_mounted:           PASS swapfs is not mounted
check_dpdk_modules:           PASS Module igb_uio.ko is available for 3.10.0-1160.el7.x86_64
check_dpdk_modules:           PASS Module rte_kni.ko is available for 3.10.0-1160.el7.x86_64
check_disk_space:             PASS FS / total=30G >= 25 GB
check_sudoers:                PASS UnCommented 'Defaults requiretty' not found found in /etc/sudoers
check_dumps:                  PASS /proc/sys/kernel/core_pattern -- 128T Pattern DETECTED
check_hugepage_alloc:         PASS Huge pages pre-allocated for 3.10.0-1160.el7.x86_64
check_ecc_memory:             PASS All Memory Modules are ECC
check_resolvable_hostname:    PASS System hostname 'mwiget-router-eu-dfw-0' is resolvable
==================================
All 128T Tests:               PASS 
```

```
$ ssh admin@151.139.87.66
Last login: Wed Jan 20 12:35:26 2021 from 212-51-142-174.fiber7.init7.net
Starting the PCLI...
admin@MyRouter.MyRouter# shell            
[admin@mwiget-router-eu-fra-0 ~]$ sudo dpdk-devbind.py --status

Network devices using kernel driver
===================================
0000:04:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' if=net2 drv=LiquidIO_VF unused=liquidio_vf,igb_uio *Active*
0000:05:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' if=eth0 drv=LiquidIO_VF unused=liquidio_vf,igb_uio *Active*
0000:06:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' if=net1 drv=LiquidIO_VF unused=liquidio_vf,igb_uio *Active*

No 'VMBus' devices detected
===========================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================
```

Connection to salt server on conductor is established!

```
root@mwiget-router-eu-fra-0 128technology]# netstat -tunap | grep 185.85.196.88
tcp        0      0 10.128.160.6:56496      185.85.196.88:4506      ESTABLISHED 2065/python3        
```

## Troubleshooting

`terraform console` offers low level, interactive access for evaluation and experimentation with expressions. It also allows to dump objects created,
  making it a good candidate to find possible expressions used for output:

```
$ echo "stackpath_compute_workload.router" | terraform console
```

## Notes

Created routers via script [create_routers.sh](create_routers.sh) and the instance got 3 network interfaces:

```
[root@mwiget-router-global-dfw-0 var]# ifconfig 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1450
        inet 10.128.16.4  netmask 255.255.240.0  broadcast 10.128.31.255
        inet6 fe80::858:aff:fe80:1004  prefixlen 64  scopeid 0x20<link>
        ether 0a:58:0a:80:10:04  txqueuelen 1000  (Ethernet)
        RX packets 2109  bytes 222003 (216.7 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2064  bytes 227956 (222.6 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 6  bytes 416 (416.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 6  bytes 416 (416.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo:1: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 185.85.196.88  netmask 255.255.255.255
        loop  txqueuelen 1000  (Local Loopback)

net1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1450
        inet 192.168.32.4  netmask 255.255.255.255  broadcast 192.168.32.4
        inet6 fe80::858:c0ff:fea8:2004  prefixlen 64  scopeid 0x20<link>
        ether 0a:58:c0:a8:20:04  txqueuelen 1000  (Ethernet)
        RX packets 5095  bytes 373585 (364.8 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 14  bytes 908 (908.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

net2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1450
        inet 172.16.48.4  netmask 255.255.255.255  broadcast 172.16.48.4
        inet6 fe80::858:acff:fe10:3004  prefixlen 64  scopeid 0x20<link>
        ether 0a:58:ac:10:30:04  txqueuelen 1000  (Ethernet)
        RX packets 468  bytes 30707 (29.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 14  bytes 908 (908.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

But the 128T router requires a public IP on one of its interfaces in order to be a hub router for nodes behind NAT.

```
[root@vorstadt-odyssey t128]# dpdk-devbind.py --status

Network devices using kernel driver
===================================
0000:00:0c.0 'Device 31dc' if=wlo2 drv=iwlwifi unused=igb_uio
0000:02:00.0 'I211 Gigabit Network Connection 1539' if=enp2s0 drv=igb unused=igb_uio *Active*
0000:03:00.0 'I211 Gigabit Network Connection 1539' if=enp3s0 drv=igb unused=igb_uio

No 'VMBus' devices detected
===========================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================
```

Issue on router:

```
admin@node1.Frankfurt# show alarms                                              
Fri 2021-01-22 12:41:52 UTC
Retrieving alarms...

========= ===================== ========== ======== =========== ================
 ID        Time                  Severity   Source   Category    Message
========= ===================== ========== ======== =========== ================
 node1:4   2021-01-22 12:41:13   INFO                INTERFACE   Intf lan1 (2)
                                                                 administrative
                                                                 ly down
 node1:5   2021-01-22 12:41:13   CRITICAL            INTERFACE   Intf lan1 (2)
                                                                 operationally
                                                                 down
 node1:7   2021-01-22 12:41:13   INFO                INTERFACE   Intf wan1 (1)
                                                                 administrative
                                                                 ly down
 node1:8   2021-01-22 12:41:13   CRITICAL            INTERFACE   Intf wan1 (1)
                                                                 operationally
                                                                 down
 node1:9   2021-01-22 12:41:46   MAJOR               SYSTEM      No
                                                                 connectivity
                                                                 to MyConductor
                                                                 .MyConductor

There are 0 shelved alarms
Completed in 0.02 seconds
```

Maybe this explain my connectivity issue:

```
[  292.581543] kni1_bridge: received packet on wan1 with own address as source address (addr:0a:58:0a:80:a0:06, vlan:0)
[  293.582559] kni1_bridge: received packet on wan1 with own address as source address (addr:0a:58:0a:80:a0:06, vlan:0)
[  294.583535] kni1_bridge: received packet on wan1 with own address as source address (addr:0a:58:0a:80:a0:06, vlan:0)
[  295.584641] kni1_bridge: received packet on wan1 with own address as source address (addr:0a:58:0a:80:a0:06, vlan:0)
[  296.585539] kni1_bridge: received packet on wan1 with own address as source address (addr:0a:58:0a:80:a0:06, vlan:0)
```

```
[root@mwiget-router-eu-fra-0 admin]# ps axwu |grep high
root      9924  0.0  0.0 112712   964 pts/0    S+   09:37   0:00 grep --color=auto high
root     11953  102  0.6 202862748 97792 ?     Sl   Jan23 1053:02 /usr/bin/highway --startup-log-level Info
[root@mwiget-router-eu-fra-0 admin]#
```

```
su
[root@mwiget-router-eu-fra-0 admin]# dpdk-devbind.py --status

Network devices using DPDK-compatible driver
============================================
0000:05:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' drv=igb_uio unused=liquidio_vf

Network devices using kernel driver
===================================
0000:04:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' if=eth0 drv=LiquidIO_VF unused=liquidio_vf,igb_uio *Active*

No 'VMBus' devices detected
```

```
[root@mwiget-router-eu-fra-0 admin]# ethtool -i eth0
driver: liquidio_vf
version: 1.7.2
firmware-version: 1.7.3
expansion-rom-version: 
bus-info: 0000:04:00.0
supports-statistics: yes
supports-test: no
supports-eeprom-access: no
supports-register-dump: yes
supports-priv-flags: no
```

```
[root@mwiget-router-eu-fra-0 admin]# ethtool -k eth0 
Features for eth0:
rx-checksumming: on
tx-checksumming: on
        tx-checksum-ipv4: on
        tx-checksum-ip-generic: off [fixed]
        tx-checksum-ipv6: on
        tx-checksum-fcoe-crc: off [fixed]
        tx-checksum-sctp: off [fixed]
scatter-gather: on
        tx-scatter-gather: on
        tx-scatter-gather-fraglist: off [fixed]
tcp-segmentation-offload: on
        tx-tcp-segmentation: on
        tx-tcp-ecn-segmentation: off [fixed]
        tx-tcp6-segmentation: on
        tx-tcp-mangleid-segmentation: off
udp-fragmentation-offload: off [fixed]
generic-segmentation-offload: on
generic-receive-offload: on
large-receive-offload: off
rx-vlan-offload: on [fixed]
tx-vlan-offload: on
ntuple-filters: off [fixed]
receive-hashing: off [fixed]
highdma: on
rx-vlan-filter: on
vlan-challenged: off [fixed]
tx-lockless: off [fixed]
netns-local: off [fixed]
tx-gso-robust: off [fixed]
tx-fcoe-segmentation: off [fixed]
tx-gre-segmentation: off [fixed]
tx-ipip-segmentation: off [fixed]
tx-sit-segmentation: off [fixed]
tx-udp_tnl-segmentation: off [fixed]
fcoe-mtu: off [fixed]
tx-nocache-copy: off
loopback: off [fixed]
rx-fcs: off [fixed]
rx-all: off [fixed]
tx-vlan-stag-hw-insert: off [fixed]
rx-vlan-stag-hw-parse: off [fixed]
rx-vlan-stag-filter: off [fixed]
busy-poll: off [fixed]
tx-gre-csum-segmentation: off [fixed]
tx-udp_tnl-csum-segmentation: off [fixed]
tx-gso-partial: off [fixed]
tx-sctp-segmentation: off [fixed]
rx-gro-hw: off [fixed]
l2-fwd-offload: off [fixed]
hw-tc-offload: off [fixed]
rx-udp_tunnel-port-offload: on
```
