#!/bin/bash
echo "Get 128T Conductor anycast IP address and update it in terraform.tfvars ..."
mv -f terraform.tfvars terraform.tfvars.old
grep -v conductor_anycast_ip terraform.tfvars.old > terraform.tfvars.new
echo "conductor_anycast_ip = $(cd ../conductor && terraform output -json conductor_anycast_ip)" >> terraform.tfvars.new
mv terraform.tfvars.new terraform.tfvars
tail -1 terraform.tfvars
