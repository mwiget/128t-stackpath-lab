resource "stackpath_compute_workload" "router" {
  name = "mwiget-router"
  slug = "mwiget-router"

  annotations = {
   # don't request an anycast IP by commenting out the following line
   #   "anycast.platform.stackpath.net" = "true"
  }

  network_interface {
   network = "default"
  }

  network_interface {
   network = "vnf-net-2"
  }

  virtual_machine {

    name = "c"
    #image = "ext-juniper-integration-31a682/centos75:1804"
    image = "ext-juniper-integration-31a682/centos75-128t:v20210123"

    resources {
      requests = {
        "cpu" = "4"
        "memory" = "16Gi"
      }
    }

    user_data = <<EOT
#cloud-config
user: admin
password: ${var.router_admin_password}
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ${var.ssh_public_key}

package_update: true
package_upgrade: false
package_reboot_if_required: true

write_files:
  - path: /tmp/preferences.json
    append: false
    content: |
      {
        "install": {
          "initialize": {
            "node-role": "combo",
            "node-name": "MyRouter",
            "router-name": "MyRouter",
            "admin-password": "${var.router_admin_password}",
            "root-password": "${var.router_root_password}",
            "conductor": {
              "primary": {
                "ip": "${var.conductor_ip}"
              }
            }
          }
        }
      }
  - path: /etc/pki/128technology/release.pem
    append: false
    content: !!binary |
      ${var.pem_128t_certificate}

runcmd:
  - install128t --preferences /tmp/preferences.json
  - systemctl enable 128T
  - reboot

final_message: "The system is ready"
EOT

  }

  target {
    name = "eu"
    min_replicas = 1
    max_replicas = 1
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "DFW", "FRA"
      ]
    }
  }
}

resource "stackpath_compute_network_policy" "router-128T" {
  name = "Allow all traffic to 128T routers"
  slug = "128t-router-network-policy"
  priority = 20000

  instance_selector {
    key = "workload.platform.stackpath.net/workload-slug"
    operator = "in"
    values = ["mwiget-router"]
  }

  policy_types = ["INGRESS"]

  ingress {
    action = "ALLOW"
    from {
      ip_block {
        cidr = "0.0.0.0/0"
      }
    }
  }
}

output "router_instances" {
  value = {
    for instance in stackpath_compute_workload.router.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "router_instance_ips" {
  value = [stackpath_compute_workload.router.instances.*.external_ip_address]
}

output "router_anycast_ip" {
  value = replace(lookup(stackpath_compute_workload.router.annotations, "anycast.platform.stackpath.net/subnets", ""), "/32", "")
}
