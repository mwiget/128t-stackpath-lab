resource "stackpath_compute_workload" "conductor" {
  name = "mwiget-conductor"
  slug = "mwiget-conductor"

  annotations = {
   # don't request an anycast IP
    "anycast.platform.stackpath.net" = "true"
  }

  network_interface {
   network = "default"
  }

  network_interface {
   network = "vnf-net-1"
  }

  virtual_machine {

    name = "c"
    image = "ext-juniper-integration-31a682/centos75:1804"

    resources {
      requests = {
        "cpu" = "4"
        "memory" = "16Gi"
      }
    }

    # The ports that should be publicly exposed on the VM.
    port {
      name = "ssh"
      port = 22
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "https"
      port = 443
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "primary"
      port = 930
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "salt-pub"
      port = 4505
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "salt-req"
      port = 4506
      protocol = "TCP"
      enable_implicit_network_policy = true
    }

    user_data = <<EOT
#cloud-config
user: admin
password: ${var.conductor_admin_password}
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ${var.ssh_public_key}

package_update: true
package_upgrade: true
package_reboot_if_required: true

write_files:
  - path: /tmp/preferences.json
    append: false
    content: |
      {
        "install": {
          "initialize": {
            "node-role": "conductor",
            "node-name": "MyConductor",
            "router-name": "MyConductor",
            "admin-password": "${var.conductor_admin_password}",
            "root-password": "${var.conductor_root_password}" 
          }
        }
      }
  - path: /etc/pki/128technology/release.pem
    append: false
    content: !!binary |
      ${var.pem_128t_certificate}

package_upgrade: false

runcmd:
  - yum -y install http://yum.128technology.com/installer/repo.rpm
  - yum -y install 128T-installer
  - install128t --preferences /tmp/preferences.json
  - systemctl enable 128T
  - reboot

final_message: "The system is finally up, after $UPTIME seconds"
EOT

  }

  target {
    name = "eu"
    min_replicas = 1
    max_replicas = 1
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "FRA"
      ]
    }
  }
}

output "conductor_instances" {
  value = {
    for instance in stackpath_compute_workload.conductor.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "conductor_instance_ips" {
  value = [stackpath_compute_workload.conductor.instances.*.external_ip_address]
}


output "conductor_anycast_ip" {
  value = replace(lookup(stackpath_compute_workload.conductor.annotations, "anycast.platform.stackpath.net/subnets", ""), "/32", "")
}

