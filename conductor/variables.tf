variable "stackpath_stack" {}
variable "stackpath_client_id" {}
variable "stackpath_client_secret" {}
variable "conductor_admin_password" {}
variable "conductor_root_password" {}
variable "ssh_public_key" {}
variable "pem_128t_certificate" {}
