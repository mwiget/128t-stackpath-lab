## Conductor

Deployed with 2 networks: default and vnf-net-1 using terraform:

```
[admin@mwiget-conductor-eu-fra-0 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet 185.85.196.71/32 brd 185.85.196.71 scope global lo:1
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 0a:58:0a:80:a0:05 brd ff:ff:ff:ff:ff:ff
    inet 10.128.160.5/20 brd 10.128.175.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::858:aff:fe80:a005/64 scope link 
       valid_lft forever preferred_lft forever
3: net1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 0a:58:c0:a8:50:04 brd ff:ff:ff:ff:ff:ff
    inet 192.168.80.4/16 brd 192.168.255.255 scope global net1
       valid_lft forever preferred_lft forever
    inet6 fe80::858:c0ff:fea8:5004/64 scope link 
       valid_lft forever preferred_lft forever
```

The IP of net1 is 192.168.80.4 and used as the conductor IP for routers deployed also in SP//.

Configuration of conductor done via UI, then shown via CLI, before adding routers:

```
$ ssh admin@151.139.87.44 show config running flat
Starting the PCLI...
Warning: Output is not to a terminal (fd=1).


config authority name               CorporateXYZ
config authority rekey-interval     24
config authority conductor-address  151.139.87.44
config authority conductor-address  192.168.80.4



config authority router MyConductor name                  MyConductor
config authority router MyConductor location              "Frankfurt Stackpath"
config authority router MyConductor location-coordinates  +50.1109+008.682127/

config authority router MyConductor node MyConductor name  MyConductor

config authority tenant corpA name  corpA

config authority tenant departmentA.corpA name  departmentA.corpA

config authority security corpA-security name                 corpA-security
config authority security corpA-security hmac-cipher          sha256
config authority security corpA-security hmac-key             (removed)
config authority security corpA-security encryption-cipher    aes-cbc-256
config authority security corpA-security encryption-key       (removed)
config authority security corpA-security encryption-iv        (removed)
config authority security corpA-security adaptive-encryption  false

config authority service Leaving-the-VPN name            Leaving-the-VPN
config authority service Leaving-the-VPN scope           public
config authority service Leaving-the-VPN security        corpA-security
config authority service Leaving-the-VPN address         0.0.0.0/0

config authority service Leaving-the-VPN access-policy corpA source  corpA
config authority service Leaving-the-VPN service-policy  _conductor_
```

After configuring the router in the same location:

```
$ ssh admin@151.139.87.44 show config running flat
Starting the PCLI...


config authority name               CorporateXYZ
config authority rekey-interval     24
config authority conductor-address  151.139.87.44
config authority conductor-address  192.168.80.4



config authority router hub1 name                  hub1
config authority router hub1 location              "Frankfurt Stackpath Pod"
config authority router hub1 location-coordinates  +50.110924+008.682127/
config authority router hub1 description           "hub with public IP"
config authority router hub1 inter-node-security   corpA-security

config authority router hub1 node node1 name              node1
config authority router hub1 node node1 asset-id          mwiget-router-eu-fra-0
config authority router hub1 node node1 role              combo

config authority router hub1 node node1 device-interface wan1 name               wan1
config authority router hub1 node node1 device-interface wan1 pci-address        0000:05:00.0

config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 name                   wan1-if1
config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 global-id              1

config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 neighborhood wan-path name      wan-path
config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 neighborhood wan-path topology  hub
config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 inter-router-security  corpA-security
config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 source-nat             true

config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 address 10.128.160.6 ip-address     10.128.160.6
config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 address 10.128.160.6 prefix-length  20
config authority router hub1 node node1 device-interface wan1 network-interface wan1-if1 address 10.128.160.6 gateway        10.128.160.1

config authority router hub1 routing default-instance type  default-instance

config authority router hub1 service-route to-internet name          to-internet
config authority router hub1 service-route to-internet service-name  Leaving-the-VPN

config authority router hub1 service-route to-internet next-hop node1 wan1-if1 node-name   node1
config authority router hub1 service-route to-internet next-hop node1 wan1-if1 interface   wan1-if1
config authority router hub1 service-route to-internet next-hop node1 wan1-if1 gateway-ip  10.128.160.1

config authority router MyConductor name                  MyConductor
config authority router MyConductor location              "Frankfurt Stackpath"
config authority router MyConductor location-coordinates  +50.1109+008.682127/

config authority router MyConductor node MyConductor name  MyConductor

config authority tenant corpA name  corpA

config authority tenant departmentA.corpA name  departmentA.corpA

config authority security corpA-security name                 corpA-security
config authority security corpA-security hmac-cipher          sha256
config authority security corpA-security hmac-key             (removed)
config authority security corpA-security encryption-cipher    aes-cbc-256
config authority security corpA-security encryption-key       (removed)
config authority security corpA-security encryption-iv        (removed)
config authority security corpA-security adaptive-encryption  false

config authority service Leaving-the-VPN name            Leaving-the-VPN
config authority service Leaving-the-VPN scope           public
config authority service Leaving-the-VPN security        corpA-security
config authority service Leaving-the-VPN address         0.0.0.0/0

config authority service Leaving-the-VPN access-policy corpA source  corpA
config authority service Leaving-the-VPN service-policy  _conductor_
```

