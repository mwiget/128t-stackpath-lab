# 128t Stackpath Lab

Goal of this lab is to automatically deploy 128T HA Conductor and Routers across regions using Terraform
on Stackpath Edge Cloud.

## Requirements

- Terraform: Follow instructions at https://learn.hashicorp.com/tutorials/terraform/install-cli#install-terraform
- `git` and `make` (if not already present), e.g. on Ubuntu, use `sudo apt-get install git make`.
- Python3 modules jxmlease, lxml and junos-eznc. install with `sudo pip3 install jxmlease lxml junos-eznc`
- `jq`: command-line JSON processor. Install with `sudo apt-get install jq`
- Stackpath Edge Cloud Stack credentials
- Local clone of this repo

## Preparation

### Clone this repo

to your working directory and change into the created folder:

```
$ git clone https://gitlab.com/mwiget/128t-stackpath-lab
$ cd 128t-stackpath-lab
```

### Stackpath stack and client id

Create or use an existing account with https://www.stackpath.com/ (credit card required). Once logged in, go to
"API Management" and create credentials. Use the terraform.tfvars.sample file as example to create your very own
terraform.tfvars file and fill in the Client ID and API Client Secret. To fill in the variable stackpath_stack in 
the same file, click on "API Management", then go to 'Stacks' tab on the left and copy the content of 'Slug' from
the desired stack. Unless you created a new stack, this will be similar to 'my-default-stack-xxxxx'.

Set the admin and root password in the file. The administrator password must be at least 8 characters long, contain at least 2 uppercase letter, at least 2 lowercase letter, at least 1 number, and cannot repeat characters more than 3 times. Convert the password into a a salted SHA-512 hash of the password and store it in the terraform.tfvars file.

Generate SHA-512 string from your password via CLI:

```
$ openssl passwd -6 My-Very-Secret-Pwd-123
$6$yw5MXvh3a2sJK.0W$vaLg9KYzBTwkrkSsvTgS39muDIguIxpeo/z0qQqn7HYTW1Ciq3F9wNKnbOq7Vkjcmv9L2tYql4Q.Gegw7FxSW.
```

```
$ cp terraform.tfvars.sample terraform.tfvars

$ cat terraform.tfvars
stackpath_stack = "<stack id>"
stackpath_client_id = "<client id>"
stackpath_client_secret = "<client secret>"
ssh_public_key = "<ssh public key>"
conductor_admin_password = "hb9rwm-Gogo-73"
conductor_root_password = "hb9rwm-Gogo-73"
pem_128t_certificate = <<-EOF
<my_base64_one_line_certificate>
EOF

$ vi terraform.tfvars
```

### 128T Conductor

The declarative description of the conductor VMs is defined in 
[conductor.tf](conductor.tf). Check the provider documentation for stackpath at
https://registry.terraform.io/providers/stackpath/stackpath/latest/docs for details.

Minimal requirements for a conductor, according to https://docs.128technology.com/docs/about_supported_platforms#conductor 
is 4 cores (HT enabled) with 8GB of RAM and 64GB storage.

Cloud-init part of the file adds the 128T repo and package, then launches installer128t with /tmp/preferences.json created.

Encode the 128T certificate into one line and add that to terraform.tfvars:

```
$ base64 -w0 < my_128t_certificate.pem > x.pem
```

Initialize and launch the Conductor with

```
$ terraform init
$ terraform apply -auto-approve
```

Use `terraform refresh` to show progress of VM deployment, until it reports 'RUNNING'. Then log into the VM using the
accounts set in conductor.tf (e.g. 128t) and monitor progress of cloud-init with the following command:

```
$ terraform refresh
stackpath_compute_workload.conductor: Refreshing state... [id=........-....-....-....-............]

Outputs:

anycast_ip = "185.85.196.93"
instance_ips = [
  tolist([
    "151.139.87.66",
  ]),
]
instances = {
  "mwiget-conductor-eu-fra-0" = {
    "name" = "mwiget-conductor-eu-fra-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.160.5"
    "public_ip" = "151.139.87.66"
  }
}
```

```
$ ssh admin@151.139.87.66
$ sudo tail -f /var/log/cloud-init.log
. . . 
2021-01-17 07:56:41,235 - handlers.py[DEBUG]: finish: modules-final/config-power-state-change: SUCCESS: config-power-state-change ran successfully
2021-01-17 07:56:41,235 - main.py[DEBUG]: Ran 10 modules with 0 failures
2021-01-17 07:56:41,237 - util.py[DEBUG]: Creating symbolic link from '/run/cloud-init/result.json' => '../../var/lib/cloud/data/result.json'
2021-01-17 07:56:41,237 - util.py[DEBUG]: Reading from /proc/uptime (quiet=False)
2021-01-17 07:56:41,238 - util.py[DEBUG]: Read 16 bytes from /proc/uptime
2021-01-17 07:56:41,238 - util.py[DEBUG]: cloud-init mode 'modules' took 2230.146 seconds (2230.15)
2021-01-17 07:56:41,238 - handlers.py[DEBUG]: finish: modules-final: SUCCESS: running modules for final
```

```
$ sudo tail -f /var/log/messages
. . .
Jan 17 07:56:41 mwiget-conductor-eu-fra-0 cloud-init: The system is finally up, after 2262.72 seconds
Jan 17 07:56:41 mwiget-conductor-eu-fra-0 systemd: Started Execute cloud user/final scripts.
Jan 17 07:56:41 mwiget-conductor-eu-fra-0 systemd: Reached target Cloud-init target.
Jan 17 07:56:41 mwiget-conductor-eu-fra-0 systemd: Startup finished in 2.043s (kernel) + 2.982s (initrd) + 37min 37.764s (userspace) = 37min 42.790s.
```

This will take several minutes. To see progress as packages are installed, run tail -f on /var/log/messages. Once its done, it automatically reboots.
If the installer failed, check /var/log/messages for any errors, e.g. by searching for json errors with `grep -i json /var/log/messages`. 

Once the system has rebooted and service initialized, you can log into the Web UI at https://<public_ip> or CLI via ssh as user admin and the 
conductor_admin_password defined in terraform.tfvars. NOTE: the host key of the system changes during the installation, so ssh client will complain
and you need to remove the cached hostkey.

```
mwiget@nuc:~/git/128t-stackpath-lab$ ssh admin@151.139.87.66
Last login: Sun Jan 17 09:34:46 2021 from 212-51-142-174.fiber7.init7.net
Starting the PCLI...
admin@MyConductor.MyConductor# show system
Sun 2021-01-17 09:49:43 UTC
Retrieving system information...

===============================
 MyConductor.MyConductor
===============================
 Status:       running
 Version:      4.5.4
 Uptime:       0 days  0:15:15
 Role:         conductor
 Alarm Count:  0

Completed in 0.08 seconds
[admin@MyConductor.MyConductor# shell
admin@mwiget-conductor-eu-fra-0 ~]$ df -h
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        7.8G     0  7.8G   0% /dev
tmpfs           7.8G  340K  7.8G   1% /dev/shm
tmpfs           7.8G  688K  7.8G   1% /run
tmpfs           7.8G     0  7.8G   0% /sys/fs/cgroup
/dev/vda1        30G  5.2G   25G  18% /
tmpfs           1.6G     0  1.6G   0% /run/user/1001
tmpfs           1.6G     0  1.6G   0% /run/user/0
[admin@mwiget-conductor-eu-fra-0 ~]$
admin@MyConductor.MyConductor# quit       
```

## Tools

To query the Stackpath Rest API (https://stackpath.dev/docs), set your credentials in the file .env (see sample [.env.sample](.env.sample))
and query the access token using the provided script [get_access_token.sh](get_access_token.sh):

```
$ ./get_access_token.sh
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1136  100   950  100   186   1552    303 --:--:-- --:--:-- --:--:--  1853
{
  "access_token": "eyJhbGciO...",
  "token_type": "bearer",
  "expires_in": 3600
}
```

## Open items

- can you turn a master into a slave conductor? Or create additional VM as a slave after the master is running (and its public/anycast IP known)
- 128T Conductor asks for 64GB SSD, but SP// VM options have a fixed 25G, plus optional persistent storage to be added at a path in the file system. 
Figure out, what folder needs more space and provision accordingly.
- SP additional networks seem to have fixed names, only 'vnf-net-1' and 'vnf-net-2' works. Picking a custom names throws an error at 'terraform apply'. Correct?
- connectivity on vnf-net-1 between instances only works currently within the same location, not between locations. Bug. Being worked on by SP//
- Need a public IP assigned to the WAN interface (eth0). Not sure 1:1 NAT helps here, as 128T does its own NAT.

## References

- Machine-to-machine Communication: https://docs.128technology.com/docs/concepts_machine_communication

