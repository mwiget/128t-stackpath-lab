#!/bin/bash
export $(grep -v '^#' .env | xargs)

ACCESS_TOKEN=$(curl --silent --request POST \
  --url https://gateway.stackpath.com/identity/v1/oauth2/token \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
    "client_id": "'"$SP_CLIENT_ID"'",
    "client_secret": "'"$SP_CLIENT_SECRET"'",
    "grant_type": "client_credentials"
  }' | jq -r '.access_token') 

STACK_ID=$(curl --silent --request GET \
  --url https://gateway.stackpath.com/stack/v1/stacks \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer $ACCESS_TOKEN" | jq -r '.results[0].id')

curl --silent --request GET \
  --url https://gateway.stackpath.com/workload/v1/stacks/$STACK_ID/workloads \
  --header "Accept: application/json" \
  --header "Authorization: Bearer $ACCESS_TOKEN" | jq

exit

curl -X GET https://gateway.stackpath.com/workload/v1/stacks/$SP_STACK_ID/workloads/$SP_WORKLOAD_ID/instances \
-H "Authorization: bearer $SP_ACCESS_TOKEN" \
-H "Content-Type: application/json" | jq
