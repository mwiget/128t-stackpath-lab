variable "stackpath_stack" {}
variable "stackpath_client_id" {}
variable "stackpath_client_secret" {}
variable "router_admin_password" {}
variable "router_t128_password" {}
variable "ssh_public_key" {}
variable "pem_128t_certificate" {}
variable "conductor_ip" {}
