#!/usr/bin/env python3

import json
import ipapi
import subprocess
import pprint
import ipcalc
import sys

instances = subprocess.check_output(
    ['terraform', 'output', '-json', 't128router_instances'])
router_dict = json.loads(instances)
# pprint.pprint(router_dict)

conductor_ip = subprocess.check_output(
    ['terraform', 'output', '-raw', 'conductor_ip']).decode('ascii')

for key in router_dict:
    router = router_dict[key]
    public_ip = router_dict[key]['public_ip']
    private_ip = router_dict[key]['private_ip']
    private_subnet = ipcalc.Network(private_ip + "/20")
    private_gateway = ipcalc.Network(str(private_subnet.network()) + "/20")[1]
    ip = ipapi.location(ip=public_ip)
    asset_id = subprocess.check_output(['ssh', "admin@" + public_ip, '-o', 'StrictHostKeyChecking=no', 'shell', 'hostname']).decode('ascii').split()
    asset_id = asset_id[-1]
    print("config authority router {name} name {name}".format(name=key))
    print("config authority router {name} location \"{city}, {country_name}\"".format(
        name=key, city=ip['city'], country_name=ip['country_name']))
    print("config authority router {name} description \"{org}, {asn}\"".format(
        name=key, org=ip['org'], asn=ip['asn']))
    print("config authority router {name} location-coordinates {lat:+08.4F}{lon:+09.4F}/".format(
        name=key, lat=ip['latitude'], lon=ip['longitude']))
    print("config authority router {name} conductor-address {ip}".format(name=key, ip=conductor_ip))
    print("config authority router {name} inter-node-security corpSecurity".format(name=key))

    print("config authority router {name} node node1 name node1".format(name=key))
    print("config authority router {name} node node1 role combo".format(name=key))
    print("config authority router {name} node node1 asset-id {id}".format(name=key, id=asset_id))

    print("config authority router {name} node node1 device-interface wan1 name wan1".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 type ethernet".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 pci-address 0000:04:00.0".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 strip-vlan true".format(name=key))

    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 name wan1".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 vlan 0".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 type shared".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 conductor true".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 neighborhood internet name internet".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 neighborhood internet topology hub".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 neighborhood internet external-nat-address {ip}".format(name=key, ip=public_ip))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 inter-router-security corpSecurity".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 source-nat true".format(name=key))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 address {ip} ip-address {ip}".format( name=key, ip=private_ip))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 address {ip} prefix-length 20".format(name=key, ip=private_ip))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 address {ip} gateway {gw}".format(name=key, ip=private_ip, gw=private_gateway))
    print("config authority router {name} node node1 device-interface wan1 network-interface wan1 icmp allow".format(name=key))

    print("config authority router {name} service-route internet-route name internet-route".format(name=key))
    print("config authority router {name} service-route internet-route service-name internet".format(name=key))
    print("config authority router {name} service-route internet-route next-hop node1 wan1 node-name node1".format(name=key))
    print("config authority router {name} service-route internet-route next-hop node1 wan1 interface wan1".format(name=key))

print("commit force")
