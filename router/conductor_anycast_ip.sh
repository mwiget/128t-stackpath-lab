#!/bin/bash
# queries the conductor anycast IP via its terraform template

cd ../conductor && terraform output -raw conductor_anycast_ip
echo ""
