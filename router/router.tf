resource "stackpath_compute_workload" "t128router" {
  name = "mwiget-t128router"
  slug = "mwiget-t128router"

  annotations = {
   # don't request an anycast IP by commenting out the following line
   "anycast.platform.stackpath.net" = "true"
  }

  network_interface {
   network = "default"
  }

  network_interface {
   network = "vnf-net-1"
  }

  virtual_machine {

    name = "c"
    image = "ext-juniper-integration-31a682/jnpr-128t-sp-tty-2:4.5.5"

    resources {
      requests = {
        "cpu" = "4"
        "memory" = "16Gi"
      }
    }

    user_data = <<EOT
#cloud-config
user: admin
password: ${var.router_admin_password}
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ${var.ssh_public_key}

users:
  - name: admin
    passwd: ${var.router_admin_password}
    ssh_authorized_keys:
      - ${var.ssh_public_key}
  - name: t128
    passwd: ${var.router_t128_password}
    ssh_authorized_keys:
      - ${var.ssh_public_key}

package_update: true
package_upgrade: false
package_reboot_if_required: true

write_files:
  - path: /tmp/preferences.json
    append: false
    content: |
      {
         "node-role": "combo",
         "node-name": "MyRouter",
         "router-name": "MyRouter",
         "admin-password": "${var.router_admin_password}",
         "root-password": "${var.router_admin_password}",
         "conductor": {
           "primary": {
             "ip": "${var.conductor_ip}"
           }
         }
       }
  - path: /etc/pki/128technology/release.pem
    append: false
    content: !!binary |
      ${var.pem_128t_certificate}


runcmd:
  - echo $HOSTNAME-$(cat /sys/devices/virtual/dmi/id/product_uuid) >/etc/hostname
  - hostname `cat /etc/hostname`
  - cp -f /etc/hostname /etc/salt/minion_id
  - sed -i s/10\.10\.10\.10/${var.conductor_ip}/ /etc/salt/minion
#  - initialize128t --preferences /tmp/preferences.json
#  - systemctl enable 128T
#  - reboot

final_message: "The system is ready"
EOT

  }

  target {
    name = "us"
    min_replicas = 1
    max_replicas = 1
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "DFW"
      ]
    }
  }
}

resource "stackpath_compute_network_policy" "t128router" {
  name = "Allow all traffic to 128T trouters"
  slug = "t128router-network-policy"
  priority = 21234

  instance_selector {
    key = "workload.platform.stackpath.net/workload-slug"
    operator = "in"
    values = ["mwiget-t128router"]
  }

  policy_types = ["INGRESS"]

  ingress {
    action = "ALLOW"
    from {
      ip_block {
        cidr = "0.0.0.0/0"
      }
    }
  }
}

output "t128router_instances" {
  value = {
    for instance in stackpath_compute_workload.t128router.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "t128router_instance_ips" {
  value = [stackpath_compute_workload.t128router.instances.*.external_ip_address]
}

output "t128router_anycast_ip" {
  value = replace(lookup(stackpath_compute_workload.t128router.annotations, "anycast.platform.stackpath.net/subnets", ""), "/32", "")
}

output "conductor_ip" {
  value = var.conductor_ip
}
