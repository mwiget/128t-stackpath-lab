
```
admin@node1.mwiget-t128router-eu-fra-0#

ast login: Thu Jan 28 18:15:12 on ttyS0
[  866.557204] rte_kni: Creating kni...
[  866.981925] LiquidIO_VF 0000:04:00.0: eth0 interface is stopped
Starting the PCLI...
[  867.634259] LiquidIO_VF 0000:04:00.0: Device removed
[  868.072638] igb_uio 0000:04:00.0: mapping 1K dma=0x4165d7000 host=ffff8a38d65d7000
[  868.079754] igb_uio 0000:04:00.0: unmapping 1K dma=0x4165d7000 host=ffff8a38d65d7000
[  869.080692] igb_uio 0000:04:00.0: uio device registered with irq 42

Broadcast message from systemd-journald@mwiget-t128router-eu-fra-0-5D3D9F8E-B901-41AA-857B-9D05BED02E9B (Thu 2021-01-28 18:16:12 UTC):

highway[17608]: Failed to Add device interface wan1, disabling the resource due to :Cannot convert value: -22

[  869.634851] IPv6: ADDRCONF(NETDEV_UP): kni254: link is not ready
[  869.641677] IPv6: ADDRCONF(NETDEV_CHANGE): kni254: link becomes ready

```

config on this router (running in SP//):

```
admin@node1.mwiget-t128router-eu-fra-0# show conf running flat                  


config authority name               stackpath128t
config authority rekey-interval     24
config authority conductor-address  151.139.87.83



config authority router mwiget-t128router-eu-fra-0 name                  mwiget-t128router-eu-fra-0
config authority router mwiget-t128router-eu-fra-0 location              "Frankfurt am Main, Germany"
config authority router mwiget-t128router-eu-fra-0 location-coordinates  +50.1188+008.6843/
config authority router mwiget-t128router-eu-fra-0 description           "StackPath LLC, AS12989"
config authority router mwiget-t128router-eu-fra-0 conductor-address     10.128.160.5
config authority router mwiget-t128router-eu-fra-0 inter-node-security   corpSecurity

config authority router mwiget-t128router-eu-fra-0 node node1 name              node1
config authority router mwiget-t128router-eu-fra-0 node node1 asset-id          mwiget-t128router-eu-fra-0-5D3D9F8E-B901-41AA-857B-9D05BED02E9B

config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 name               wan1
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 type               ethernet
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 pci-address        0000:04:00.0
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 strip-vlan         true

config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 name                   wan1
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 global-id              5
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 vlan                   0
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 type                   shared
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 conductor              true

config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 neighborhood internet name                  internet
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 neighborhood internet topology              hub
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 neighborhood internet external-nat-address  151.139.87.37
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 inter-router-security  corpSecurity
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 source-nat             true

config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 address 10.128.160.7 ip-address     10.128.160.7
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 address 10.128.160.7 prefix-length  20
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 address 10.128.160.7 gateway        10.128.160.1
config authority router mwiget-t128router-eu-fra-0 node node1 device-interface wan1 network-interface wan1 icmp                   allow

config authority router mwiget-t128router-eu-fra-0 service-route internet-route name          internet-route
config authority router mwiget-t128router-eu-fra-0 service-route internet-route service-name  internet

config authority router mwiget-t128router-eu-fra-0 service-route internet-route next-hop node1 wan1 node-name  node1
config authority router mwiget-t128router-eu-fra-0 service-route internet-route next-hop node1 wan1 interface  wan1

config authority tenant corp name      corp
config authority tenant corp security  internal

config authority security corpSecurity name               corpSecurity
config authority security corpSecurity hmac-cipher        sha256
config authority security corpSecurity hmac-key           (removed)
config authority security corpSecurity encryption-cipher  aes-cbc-256
config authority security corpSecurity encryption-key     (removed)
config authority security corpSecurity encryption-iv      (removed)

config authority service internet name           internet
config authority service internet scope          public
config authority service internet security       internal
config authority service internet address        0.0.0.0/0

config authority service internet access-policy corp source  corp
```

Output of 128tok.sh:

```
$ 128tok.sh

128T System Information:
-----------------------------
Kernel version:               3.10.0-1160.el7.x86_64
Total Cores:                  4
Assigned Cores:               3
Unassigned(DPDK):             1
Context:                      run
Derived Role:                 combo
Specified Role:               
Source:                       all

128T System Checks:
-----------------------------
check_cpu_vendor:             PASS CPU VendorID: GenuineIntel (must be Intel)
check_cpu_flags:              PASS CPU 0 core 0 has flags ssse3,sse4_1,sse4_2
check_cpu_flags:              PASS CPU 1 core 1 has flags ssse3,sse4_1,sse4_2
check_cpu_flags:              PASS CPU 2 core 2 has flags ssse3,sse4_1,sse4_2
check_cpu_flags:              PASS CPU 3 core 3 has flags ssse3,sse4_1,sse4_2
check_selinux_status:         PASS SELinux status is cfg:disabled,run:disabled
check_system_memory:          PASS 16 GB of Required Minimum of 3 GB
check_swap_mounted:           PASS swapfs is not mounted
check_dpdk_modules:           PASS Module igb_uio.ko is available for 3.10.0-1160.el7.x86_64
check_dpdk_modules:           PASS Module rte_kni.ko is available for 3.10.0-1160.el7.x86_64
check_disk_space:             PASS FS / total=30G >= 25 GB
check_sudoers:                PASS UnCommented 'Defaults requiretty' not found found in /etc/sudoers
check_dumps:                  PASS /proc/sys/kernel/core_pattern -- 128T Pattern DETECTED
check_hugepage_alloc:         PASS Huge pages pre-allocated for 3.10.0-1160.el7.x86_64
check_ecc_memory:             PASS All Memory Modules are ECC
check_resolvable_hostname:    PASS System hostname 'mwiget-t128router-eu-fra-0-5D3D9F8E-B901-41AA-857B-9D05BED02E9B' is resolvable
==================================
All 128T Tests:               PASS 
$
```

