## Deploy one or more 128T routers on SP//

### Requirements

```
sudo pip3 install ipcalc
sudo pip3 install ipapi
```

### Deploy

```
$ terraform apply --auto-approve
```

After a few minutes, the instances are configured and up and running:


Checking on the conductor:

```
$ terraform refresh
stackpath_compute_network_policy.t128router: Refreshing state... [id=8246149e-f680-4c13-9b22-9b1ce321c469]
stackpath_compute_workload.t128router: Refreshing state... [id=a680ac5f-ee44-4d2e-a367-48a2dedac81a]

Outputs:

t128router_anycast_ip = "185.85.196.100"
t128router_instance_ips = [
  tolist([
    "151.139.55.77",
    "151.139.87.37",
  ]),
]
t128router_instances = {
  "mwiget-t128router-eu-dfw-0" = {
    "name" = "mwiget-t128router-eu-dfw-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.16.5"
    "public_ip" = "151.139.55.77"
  }
  "mwiget-t128router-eu-fra-0" = {
    "name" = "mwiget-t128router-eu-fra-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.160.7"
    "public_ip" = "151.139.87.37"
  }
}
```

Now check the assets on the conductor. First, get the conductors IP:

```
$ ./conductor_anycast_ip.sh 
Get 128T Conductor anycast IP address and update it in terraform.tfvars ...
conductor_anycast_ip = "185.85.196.68"
```

Then query its assets:

```
$ ssh admin@185.85.196.68 show assets


Starting the PCLI...
Thu 2021-01-28 09:40:33 UTC

============== ============== ====================================================================================================== ============== ============== ================ ========
 Router         Node           Asset Id                                                                                               128T Version   Status         Time in Status   Errors
============== ============== ====================================================================================================== ============== ============== ================ ========
. . .
                None           mwiget-t128router-eu-dfw-0-5269E8AB-1896-48B9-99B3-59A56AADB44A                                        None           Pending        2m 23s                0
                None           mwiget-t128router-eu-fra-0-6FE4DD38-B334-4A29-B360-5BA20BCAA9EC                                        None           Pending        3m 9s                 0
. . .

Completed in 0.06 seconds
```

Check the PCI addresses of eth0, they should be the same for all routers:

```
$ ssh admin@151.139.87.37 shell sudo -- /bin/dpdk-devbind.py --status
Starting the PCLI...
Warning: Output is not to a terminal (fd=1).

Network devices using kernel driver
===================================
0000:04:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' if=eth0 drv=LiquidIO_VF unused=liquidio_vf,igb_uio *Active*

No 'VMBus' devices detected
===========================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================
```

And for the other one:

```
$ ssh admin@151.139.55.77 shell sudo -- /bin/dpdk-devbind.py --status
The authenticity of host '151.139.55.77 (151.139.55.77)' can't be established.
RSA key fingerprint is SHA256:E2mnCND6f9+gxENJNJ1LGEHj4SiMaij2OxoQzT0uIY4.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '151.139.55.77' (RSA) to the list of known hosts.
Starting the PCLI...
Warning: Output is not to a terminal (fd=1).

Network devices using kernel driver
===================================
0000:04:00.0 'CN23XX [LiquidIO II] SRIOV Virtual Function 9712' if=eth0 drv=LiquidIO_VF unused=liquidio_vf,igb_uio *Active*

No 'VMBus' devices detected
===========================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================
```

Auto-generate config for the launched routers:

```
$ ./generate_config.py  > router.cfg.txt
```

Example file: [router.cfg.txt](router.cfg.txt)


And apply it to the conductor via ssh:

```
$ ./generate_config.py | ssh admin@<conductor_ip>
```

![](routers.jpg)

## TODO

- Need to randomize admin password. It gets overwritten from /etc/128technology/user-factory-defaults.xml, set in the generated VM image.


## launch via multipass 

(running the .qcow2.gz compressed image failed)

```
$ multipass launch file:///home/mwiget/git/128t-stackpath-lab/router/StackPath-4.5.5.LiquidIO.25G.qcow2
```

```
$ multipass list
Name                    State             IPv4             Image
welcome-dachshund       Running           10.56.97.78      Not Available
```

```
$ multipass shell welcome-dachshund
Last login: Fri Feb  5 11:21:33 2021 from 10.56.97.1

+---------------------------------------------------------------------+
|  Welcome to:                                                        |
|   _ ____  ___  _            _                 _                     |
|  / |___ \( _ )| |_ ___  ___| |__  _ __   ___ | | ___   __ _ _   _   |
|  | | __) / _ \| __/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | | | |  |
|  | |/ __/ (_) | ||  __/ (__| | | | | | | (_) | | (_) | (_| | |_| |  |
|  |_|_____\___/ \__\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |\__, |  |
|                                                       |___/ |___/   |
|  Secure Vector Routing ...                                          |
|                                                                     |
+---------------------------------------------------------------------+
```
