#!/bin/bash

if [ ! -e /etc/pki/128technology/release.pem ]; then
  echo "You must install your certificate for 128Technology first in /etc/pki/128technology/release.pem"
  exit 1
fi

curl -s --cert /etc/pki/128technology/release.pem https://yum.128technology.com/isos/ |grep -Po '(?<=href=")[^"]*.iso' |sort

echo ""
echo "to download, use the following example:"
echo "curl -O --cert /etc/pki/128technology/release.pem https://yum.128technology.com/isos/128T-<VERSION>.el7.x86_64.iso"

echo "Example to download OTP 4.5.4-1 iso:"
echo "curl -O --cert /etc/pki/128technology/release.pem https://yum.128technology.com/isos/128T-4.5.4-1.el7.OTP.v1.x86_64.iso"
