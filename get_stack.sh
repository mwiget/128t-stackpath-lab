#!/bin/bash
export $(grep -v '^#' .env | xargs)
ACCESS_TOKEN=$(curl --silent --request POST \
  --url https://gateway.stackpath.com/identity/v1/oauth2/token \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
    "client_id": "'"$SP_CLIENT_ID"'",
    "client_secret": "'"$SP_CLIENT_SECRET"'",
    "grant_type": "client_credentials"
  }' | jq -r '.access_token') 

curl --silent --request GET \
  --url https://gateway.stackpath.com/stack/v1/stacks \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer $ACCESS_TOKEN" | jq
